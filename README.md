# layout

Build layouts with minimal coding.

## Purpose and Usage

The library offers a convenient shorthand for laying out complex, responsive layouts in Dart using Google's Flutter framework.  Space is divided evenly between elements by default (dynamic flex argument soon to come), and the layout will change automatically depending on screen size (and orientation) with no work required by the code author.

For example, here is a responsive layout that covers the full width of the device screen.  It shows two elements, side-by-side, and each taking up 50% of the screen width - the number 1 is displayed in the left box, the number 2 in the right.


```
new Expanded(
	child: new Row(
    	children: [
        	new Expanded(child: Column(children: [
            	new Text('1')
            ]),
            new Expanded(child: new Column(children: [
            	new Text('2')
            ])
        ]
    )
)
```
That's an awful lot of typing.  With the layout library, it is as simple as:
```
new Layout([['1', '2']])
```
One set of brackets indicates rows.  Two sets indicate columns.  

## Complicated Example
### This would take hundreds of lines to accomplish without the Layout library.
This example includes the Cell class - it allows for easy specification of a layout element's padding and color, and also centers its contents by default (you can disable this by passing `center: false`). It accepts the same array-style metasyntax as the Layout class.
Also demonstrated are several utilities offered by the library, such as automatic typecasting (numbers and strings are automatically converted to Text widgets), no use of the "child" parameter to save time.
 
 As a bonus, it is possible to code your layout more visually using this array metasyntax - notice how the numbers 1, 2, 3... in the code sample were intentionally arranged in a way that is visually representative of what is displayed in the screenshots.
```
return Scaffold(
        appBar: AppBar(
          title: Text('Test'),
        ),
        body: Layout([  // Rows
          Cell([  // Rows
            1,
            '2'
          ], color: Colors.blue),
          Cell([[  // Columns
              '3', '4',
              Cell([  // Rows (as a column)
                '5',
                '6'
              ], padding: 32, color: Colors.blue)
            ]], color: Colors.red, padding: 16) 
        ]));
```

The included example (located in `/example/`) illustrates newly added flex functionality for Cell widgets and building stateful layouts.  

## Pull requests are encouraged.  This package was developed to speed up UI design prototyping, and I am not a professional developer with the skills to maintain or tidy up this package.

### Portrait (Tablet)
![alt text](https://i.imgur.com/Sz5g5QS.png "Portrait")
### Landscape (Tablet)
![alt text](https://i.imgur.com/JjlScP0.png "Landscape")
