import 'package:flutter/material.dart';

class Cell extends StatefulWidget {
  Cell(this.content,
      {this.color,
      this.padding = 0,
      this.center = true,
      this.elevation = 0,
      this.flex: 1});
  final dynamic content;
  final Color color;
  final dynamic padding;
  final bool center;
  final dynamic flex;
  final dynamic elevation;

  @override
  _CellState createState() => _CellState();
}

class _CellState extends State<Cell> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        child: widget.elevation > 0
            ? Material(
                elevation: widget.elevation.toDouble(),
                color: widget.color,
                child: Layout(widget.content, center: true),
              )
            : Container(
                color: widget.color,
                child: Layout(widget.content, center: true)),
        padding: widget.padding is EdgeInsetsGeometry
            ? widget.padding
            : EdgeInsets.all(widget.padding.toDouble()));
  }
}

class Layout extends StatefulWidget {
  Layout(this.data, {this.center = false});
  final dynamic data;
  final bool center;

  @override
  _LayoutState createState() => _LayoutState();
}

class _LayoutState extends State<Layout> {
  Widget _sanitize(dynamic x) =>
      (x is String || x is int || x is double) ? Text(x.toString()) : x;

  bool _checkIfColumn(List l) => l.length == 1 && l[0] is List;

  List _expand(List l) {
    // Expands contents of a List

    _center(x) => widget.center ? Center(child: _sanitize(x)) : _sanitize(x);

    return l
        .map((x) =>
//        (x is Cell && x.flex == false)
//            ? Center(child: _sanitize(x))
//            :
            Expanded(
                child: _center(x),
                flex: x is Cell && x.flex is int ? x.flex : 1))
        .toList();
  }

  Widget _parseList(List l) {
    // accept a list and return a Column or a Row

    dynamic _l;
    bool _column = _checkIfColumn(l);
    _l = _column ? l[0] : l;

    _l = _l.map((x) => x is List ? _parseData(x) : x).toList();

    return _column
        ? Column(children: [Expanded(child: Row(children: _expand(_l)))])
        : Row(children: [Expanded(child: Column(children: _expand(_l)))]);
  }

  Widget _parseData(dynamic data) =>
      data is List ? _parseList(data) : _parseList([data]);

  @override
  Widget build(BuildContext context) => _parseData(widget.data);
}
