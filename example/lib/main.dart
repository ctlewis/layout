import 'package:flutter/material.dart';
import 'package:layout/layout.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _add = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Test'),
        ),
        body: Layout([
          // Rows
          Cell([
            // Rows
            1 + _add,
            '2'
          ], color: Colors.blue, flex: 2,),
          Cell([
            [
              // Columns
              '3', '4',
              Cell([
                // Rows (as a column)
                '5',
                '6'
              ], padding: 32, color: Colors.blue)
            ]
          ], color: Colors.red, padding: 16, flex: 2),
          Cell(RaisedButton(child: Text('PRESS ME'), onPressed: () => setState(() => _add += 1),)),
        ]));
  }
}

MaterialApp _myApp = MaterialApp(
  home: MyHomePage(),
);

void main() => runApp(_myApp);
